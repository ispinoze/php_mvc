<?php

class Panel extends Controller{
	
	public function __construct() {
		parent::__construct();
		
		

		if (Session::getSession("login")==false) {
			Session::destroy(); 
			header("Location: ".SITE_URL."/admin/login") ;
		}		
	}
	
	public function index(){
	
	
	
	
	}
	
	public function home(){
		$data["homePage"]=array(
				
				"username"=>Session::getSession("username")
				
		);
		$this->load->view("panel/header",$data);
		$this->load->view("panel/left",$data);
		$this->load->view("panel/content",$data);
		$this->load->view("panel/footer",$data);
		
		
	}
	
	function AddNewContent() {
		
		$data["homePage"]=array(
		
				"username"=>Session::getSession("username")
		);
		
		$this->load->view("panel/header",$data);
		$this->load->view("panel/left",$data);
		$this->load->view("panel/AddNewContent",$data);
		$this->load->view("panel/footer",$data);
		
	}
	
}