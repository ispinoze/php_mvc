<?php

class Admin extends Controller{
	
	
	public function __construct() {
		parent::__construct();
	}
	
	public function index(){
		
		
		$this->login();
		
	}
	
	public function login() {
		
		
		if (Session::getSession("login")==true) {
			
			header("Location: ".SITE_URL."/panel") ;
		}
		$this->load->view("admin/loginform");
	}
	
	public function runlogin() {
		
		$username=$_POST["username"];
		
		$pass=$_POST["pass"];
		
		$data=array(
			":kadi"=>$username,
			":parola"=>$pass
		);
		

		$admin_model=$this->load->model("admin_model");
		
		$result=$admin_model->userControl($data);
		
		/*if ($result){

			echo $result[0]["username"];
			
		}*/
		
		if ($result) {
			Session::setSession("login", true);
			
			Session::setSession("username", $username);
			
			header("Location:".SITE_URL."/panel/home");
		}
		else{
			
			header("Location:".SITE_URL."/admin/login");
			
		}
		
		
		
		
		
	}
	
	public function logOut(){
		
		
		Session::destroy();
		
		header("Location:".SITE_URL."/admin/login");
	}
	
}