<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class load {

    public function __construct() {
     
    }
    
    public function view($filename,$data=false){
            
        if ($data==true) {
            extract($data);
        }
        
        include "app/views/".$filename."_view.php";
        
    }
    
    public function model($filename){
            
        include "app/models/".$filename.".php";
        
        return new $filename();
        
    }
}
