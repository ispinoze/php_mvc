<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Database extends PDO {

    public function __construct($dsn,$user,$passwd) {

        parent::__construct($dsn, $user, $passwd);
        
        $this->query("SET NAMES 'utf8'");
        $this->query("SET CHARACTER  SET utf8");
        
    }
    
    public function select($sql,$array=array(),$fecthMode=PDO::FETCH_ASSOC) {
        
        //$sql="SELECT * from test id=";
        $sth=$this->prepare($sql);
        
        foreach ($array as $key => $value) {
            $sth->bindValue($key, $value);
        }
        
        $sth->execute();
        
        return $sth->fetchAll($fecthMode);
        
    }
    
    public function insert($tableName,$data) {
        
        $fieldKeys=implode(",",array_keys($data));
        
        $fieldValues=":".implode(", :",  array_keys($data));

        $sql="insert into $tableName($fieldKeys) values($fieldValues)";
        
        $sth=$this->prepare($sql);
        
        foreach ($data as $key => $value) {
            
            $sth->bindValue(":$key", $value);
        }
        
       return $sth->execute();
    }
    
    public function update($tableName,$data,$where){
    	
    	$updateKeys=null;
    	
    	foreach ($data as $key => $value) {
    		$updateKeys.="$key=:$key,";
    	}
    	
    	$updateKeys=rtrim($updateKeys,",");
    	
    	$sql="UPDATE $tableName set $updateKeys where $where" ;
    	
    	$sth=$this->prepare($sql);
    	
    	foreach ($data as $key => $value) {
    	
    		$sth->bindValue(":$key", $value);
    	}
    	
    	return $sth->execute();
    	
    }
    
    
    function delete($tableName,$where) {
    	
    	$sql="delete from $tableName where $where";
    	
    	$sth=$this->prepare($sql);
    	
    	return $sth->execute();
    }
    

}